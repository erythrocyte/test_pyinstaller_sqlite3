# TEST PyInstaller with sqlite3

# requirement:
1. sqlite3 (from zypper)
2. python3 interpetator (zypper)
3. pyinstaller (pip3)


#COMMANDS TO RUN:

## VAR_1

1. pyinstaller --onefile main.py --> main.spec file will appear; (not true) 
1.1. instead : sudo pyinstaller --onefile main.py (true)
2. cd dist
3. ./main

## VAR_2

1. pyinstaller --onefile main.py --> main.spec file will appear; 
2. cd dist/
3. ./main --> error : ImportError: No module named 'work_db'
4. cd ..
5. vim main.spec
6. after ```a = Analysis(...)``` insert ```a.datas += [('work_db.py', a.pathex[0] + '/source/work_db.py', 'source')]```
7. pyinstaller main.spec
8. cd dist
9. ./main --> you ll get an error (no sqlite3)
10. cd ..
11. vim main.spec
12. insert   

		def get_sqlite3_path():   

			import sqlite3   
			sqlite3_path = sqlite3.__path__[0]   
			print('sqlite3_path = {}'.format(sqlite3_path))   
			return sqlite3_path 

	dict_tree = Tree(get_sqlite3_path(), prefix='sqlite3', excludes=["*.pyc"])   

	a.datas += dict_tree   

	a.binaries = filter(lambda x: 'sqlite3' not in x[0], a.binaries)   

13. pyinstaller main.spec --> error no module "_sqlite3"

test.db file must be created in programm directory;
