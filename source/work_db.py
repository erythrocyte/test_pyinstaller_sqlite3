#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3
from sqlite3 import Error

import os.path

def create_connection(db_file):
	""" create a database connection to a SQLite database """
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
	# finally:
		# conn.close()
	return None

def is_file_exists(fname):
	return os.path.isfile(fname)

def create_empty_db(dd):
	if (not is_file_exists(dd)):
		print ('data base {} created'.format(dd))
		conn = create_connection(dd)
		if conn is not None:
			# create projects table
			create_def_table(conn);
	else:
		print ('data base {} exists'.format(dd))

def create_def_table(conn):
	query = """ CREATE TABLE IF NOT EXISTS COMPANY (
		ID integer PRIMARY KEY,
		COMPANY_NAME text NOT NULL
		); """
	try:
		c = conn.cursor()
		c.execute(query)
	except Error as e:
		print(e)
